extends Node

var cell_size = 32
var cell_offset = cell_size/2

var debugger_text = ""
var ball_imgs =[
	'bubble-colourblind-1.png',
	'bubble-colourblind-2.png',
	'bubble-colourblind-3.png',
	'bubble-colourblind-4.png',
	'bubble-colourblind-5.png',
	'bubble-colourblind-6.png',
	'bubble-colourblind-7.png',
	'bubble-colourblind-8.png'
]
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func position_to_grid(pos):
	var y = int(pos.y/cell_size)
	var x = int((pos.x+cell_offset)/cell_size)
	var xx = int((pos.x)/cell_size)

	var grid = [x,y]

	return grid
	
func grid2pos(gpos):
	var x = gpos[0]
	var y = gpos[1]
	var pos = Vector2(x*cell_size+cell_offset,y*cell_size+cell_offset)
	if y % 2 == 0:
		pos = Vector2(x*cell_size,y*cell_size+cell_offset)
	
	return pos
	
func check_free_spot(obj):
	var balls = get_tree().get_nodes_in_group("balls")
	for ball in balls:
		if ball.grid_pos == obj.grid_pos && ball != obj:
			return false
			
	return true
	
func random_ball():
	var dir = "res://objects/balls/imgs/"
	var ball = ball_imgs[randi() % ball_imgs.size()]
	var res = dir + ball
	return res
