extends Node2D

@export var player_id = 0
var active_ball

@onready var new_ball = preload("res://objects/balls/ball.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	#load_ball()
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_left"):
		rotate(-delta)
		
	if Input.is_action_pressed("ui_right"):
		rotate(delta)
		
	if Input.is_action_pressed("ui_down"):
		var balls = get_tree().get_nodes_in_group("balls")
		for ball in balls:
			ball.status = "popped"
		
	rotation = clamp(rotation,-1.5,1.5)
	if ball_check():
		load_ball()
		
func ball_check():
	var balls = get_tree().get_nodes_in_group("balls")
	for ball in balls:
		#print(ball.status)
		if ball.status != "stopped":
			return false
	
	return true
	
	#Global.debugger_text = str(rotation)
func load_ball():
	var ball = new_ball.instantiate()
	ball.global_position = global_position
	#ball.active = false
	get_tree().current_scene.add_child(ball)
