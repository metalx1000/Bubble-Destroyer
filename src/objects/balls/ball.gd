extends Node2D

var speed = 300
var status = "loaded"

var angle = 0
@onready var shooter = get_tree().get_nodes_in_group("shooters")[0]
@onready var sprite = $sprite
var ball_type = 0
var fall_x = 0
var grid_pos = [0,0]
var grid_pos_last = [0,0]

# Called when the node enters the scene tree for the first time.
func _ready():

	randomize()
	fall_x = randf_range(-1.0,1.0)
	#position = shooter.global_position
	ball_type = randi_range(0,7)
	sprite.frame = ball_type
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if status == "popped":
		fall(delta)
		return
		
	if status == "stopped":
		return
	

		
	if status == "loaded":
		rotation = shooter.rotation
		if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_up"):
			status = "active"
				
	#if Input.is_action_pressed("ui_accept"):
	move(delta)

func move(delta):
	
	if status != "active":
		return
	grid_pos_last = grid_pos
	grid_pos = Global.position_to_grid(position)
	Global.debugger_text = grid_pos
	
	
	#position.y -= speed * delta
	position.y -= cos(deg_to_rad(rotation_degrees)) * speed * delta
	position.x += sin(deg_to_rad(rotation_degrees)) * speed * delta
	
		
func stop():
	#while !Global.check_free_spot(self):
#		grid_pos[0] -= 1
	#	grid_pos = grid_pos_last
	#	position = Global.grid2pos(grid_pos)
	
	if status == "stopped":
		return
	status = "stopped"

func fall(delta):
	fall_x = lerp(fall_x,0.0,delta)
	position.y += speed * delta
	position.x += fall_x * delta * speed / 2
	if position.y > get_viewport().get_visible_rect().size.y:
		queue_free()
		pass
		
func _on_area_2d_body_entered(body):
	if status != "active":
		return
		
	if body.is_in_group("walls"):
		rotation_degrees *= -1

	if body.is_in_group("top"):
		grid_pos[1] = 1
		grid_pos[0] = int(position.x/Global.cell_size)

		position = Global.grid2pos(grid_pos)
		#position.x += Global.cell_offset
		stop()
		
func pop():
	status = "popped"
	
func _on_area_2d_area_entered(area):
	#if area.is_in_group("balls") && area.ball_type == ball_type:
	#	area.status = "popped"
#		pop()
#		return
	
	if status != "active":
		return
	
	if area.is_in_group("balls"):
		stop()
		speed = 0
		
		grid_pos = grid_pos_last
		position = Global.grid2pos(grid_pos)
		#grid_pos = Global.position_to_grid(position)
		return
		
		#if grid_pos[0] > area.grid_pos[0]:
		#	grid_pos[0] = grid_pos[0] - 1
		
		#if grid_pos[1] == area.grid_pos[1]:
		#	grid_pos[1] = grid_pos[1] + 1
		#	grid_pos[0] -= 1
			
		#var offset = 16
		#if position.x < area.position.x:
	#		offset *= -1
			
		position = Global.grid2pos(grid_pos)
		#if grid_pos[1]%2 == 0:
		#	position.x += offset
		if !Global.check_free_spot(self):
			grid_pos = grid_pos_last
			position = Global.grid2pos(grid_pos)
			

